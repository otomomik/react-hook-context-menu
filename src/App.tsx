import { helloWorld } from 'lib'

import type { FC } from 'react'

export const App: FC = () => {
  return (
    <>
      <h1>ReactHookContextMenu</h1>
      <h2>{helloWorld()}</h2>
    </>
  )
}
